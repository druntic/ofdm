import numpy as np
from matplotlib import pyplot as plt
import sys
def symbolsToOFDMsymbol(symbols, Nfft, Npilot, pilotValue, customPilots=[]):
    OFDMsym=np.zeros(Nfft, dtype=complex)
    subcarriers=[i for i in range(Nfft)]#list of subcarrier indices
    if len(customPilots)==0:
        pilots=np.array(subcarriers[0::Nfft//Npilot])#list of pilot indices
        del subcarriers[0::int(Nfft/Npilot)]
    else:
        pilots=np.array(customPilots)
        subcarriers = [subcarriers[i] for i, e in enumerate(subcarriers) if i not in customPilots]
    subcarriers=np.array(subcarriers)
    if Npilot!=len(customPilots):
        sys.exit(f'Expected {Npilot} pilots, got {len(customPilots)}')
    #print(pilots)
    #print(pilotValue)
    OFDMsym[pilots]=pilotValue
    OFDMsym[subcarriers]=symbols
    # for x in pilots:
    #     plt.plot(x, 0, marker="o", markersize=5,color="red")
    # for x in subcarriers:
    #     plt.plot(x, 0, marker="o", markersize=5,color="blue")
    # plt.title("Subcarriers")
    # plt.show()
    return OFDMsym
    
def IFFT(OFDMsym):
    return np.fft.ifft(OFDMsym)

def FFT(rec):
    return np.fft.fft(rec)

def CPrefix(symbol, N, CPlength):
    return symbol[-int(N*CPlength):].copy()
def RemovePilots(symbols, Nfft, Npilot,customPilots=[]):
    if len(customPilots)!=0:
        return [symbols[i] for i, e in enumerate(symbols) if i not in customPilots]
    return symbols.reshape(-1,Nfft//Npilot)[:,1:].flatten()

def StandardPilots(nOFDM, Npilots):
    psi=np.ones(Npilots)
    psi[-1]=-1
    return np.sqrt(2)*np.array([psi[(nOFDM+i)%Npilots] for i in range(Npilots)])