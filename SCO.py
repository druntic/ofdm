import numpy as np

def findSFO(Z1, Z2, customPilots):
    negativePilots=customPilots[len(customPilots)//2:]
    positivePilots=customPilots[:len(customPilots)//2]
        
    phiNeg=np.angle(sum([Z2[x]*np.conjugate(Z1[x]) for x in negativePilots]))
    phiPos=np.angle(sum([Z2[x]*np.conjugate(Z1[x]) for x in positivePilots]))
    lam=80/64
    K=52
    
    eps0=1/(2*np.pi*lam)*1/(K/2+1)*(phiPos-phiNeg)
    #v0=1/(2*np.pi*eps0)*(1/(2*(1+eps0)))*(phiPos+phiNeg)
    #print("SFO: ", eps0)
    return eps0

def SFOcorrection(Z, eps0):
    for k in range(len(Z)):
        phi=+k*eps0
        #print("phi: ", round(phi,2))
        Z[k]=Z[k] * np.exp(-1j*phi)
    return Z
