import numpy as np
import math
import pylab as pyl

def BER(signal, rec):
    counter=0
    print("signal len: ", len(signal))
    print("rec len: ", len(rec))
    #return np.mean(np.abs(signal[:len(rec)] - rec))
    counter=sum([0 if x==y else 1 for x,y in zip(signal,rec[:len(signal)])])
    # for i in range(len(signal)):
    #     if signal[i]!=rec[i]:
    #         counter+=1
    return counter/len(signal)


        
def recievedSignal(signal, SNR,M, channelType, K):

    #generate noise and add it to signal
    if len(signal)!=0:
        totalPow=0
        for x in signal:
            totalPow+=np.real(x)**2+np.imag(x)**2#this is the same as abs(x)**2
        signalPower_lin=totalPow/(len(signal))
        #print("average power: ", signalPower_lin)
        signalPower_dB=10*math.log10(signalPower_lin)   
        noisePower_db=signalPower_dB - SNR
        noisePower_lin=10**(noisePower_db/10)
        noise=[math.sqrt(noisePower_lin/2)*(np.random.randn()+1j*np.random.randn()) for x in signal]
    else:
        return [math.sqrt(0.01/2)*(np.random.randn()+1j*np.random.randn()) for x in range(500)]
    
    
    if channelType=="AWGN":
        rec=[x + y for x, y in zip(signal, noise)]
    elif channelType=="Rayleigh":
        h=1/np.sqrt(2)*(pyl.randn(len(signal))+1j*pyl.randn(len(signal)))
        rec=[x + y/i for x, y,i in zip(signal, noise,h)]
    elif channelType=="Rician":
        mean=np.sqrt(K/(2*(K+1)))
        deviation=np.sqrt(1/(2*(K+1)))
        h=np.random.normal(mean,deviation**2,len(signal))+1j*np.random.normal(mean,deviation**2,len(signal))
        #print(h)
        rec=[i*x + y for x, y,i in zip(signal, noise,h)]
    return rec



def SignalPower(signal):
    totalPow=0
    for x in signal:
        totalPow+=np.real(x)**2+np.imag(x)**2#this is the same as abs(x)**2
    return totalPow/(len(signal))

def packet(signal,SNR,M, channelType, K, nPacket):
    signals=np.array([])
    for i in range(nPacket):
        noise=np.array(recievedSignal([], SNR, M, channelType, K))
        packet=np.array(recievedSignal(signal, SNR, M, channelType, K))
        signals=np.hstack([signals,noise,packet,noise ])
    return signals


