import komm as km
import modulate as mod
import numpy as np
import matplotlib.pyplot as plt
import random
import math
import itertools
import demodulate as demod
import BER as ber
if __name__ == "__main__":

    plt.yscale('log')
    
    FFT_N=48#number of subcarriers
    M=4#modulation type
    N=int(math.log2(M))*FFT_N#total number of bits
    K=10#rician K factor
    N=5000
    

    randomList=[random.randint(0, 1) for i in range(N)]

    #M=4
    Ms=[4]
    channels=["AWGN","Rayleigh","Rician"]
    #channels=["AWGN"]
    for channelType in channels:
        for M in Ms:
            print("M: ", M)
            lst = list(itertools.product([0, 1], repeat=int(math.log2(M))))
            #show constellations
            Map=[]
            for x in lst:
                for a in x:
                    Map.append(a)
            if M==2:
                cons=mod.BPSK(Map)
                signal=mod.BPSK(randomList)
            else: 
                cons=mod.QAM(Map,M)
                signal=mod.QAM(randomList,M)
            BERs=[]
            SNRs=[]
            for SNR in range(1,20,1):
                
                #generate noise and add it to signal    
                rec=ber.recievedSignal(signal, SNR, M, channelType, K)
                constructedSignal=[]
                for x in rec:
                    
                    if M==2:
                        minimum, binary = demod.dePSK(x, cons, M)
                    else:
                        minimum, binary =demod.deQAM(x,cons,M)
                    constructedSignal+=list(binary)
                if ber.BER(constructedSignal, randomList)!=0:
                    BERs.append(ber.BER(constructedSignal, randomList))
                    SNRs.append(SNR)
                else:
                    BERs.append(0)
                    SNRs.append(SNR)
                    break
                print("SNR: ",SNR,"BER", ber.BER(constructedSignal, randomList))
            plt.plot(SNRs, BERs) 

    plt.grid(True)
    
    plt.xlabel("S/N")
    plt.ylabel("BER")
    if len(channels)==1:
        plt.legend(["BPSK","QAM4","8PSK","QAM16","QAM64","QAM256"])
    else:
        plt.legend(["AWGN", "Rayleigh","Rician, K = "+str(K)])
    plt.show()
    