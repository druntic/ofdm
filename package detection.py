import numpy as np

def detectPackage(signal,t, CP, Nfft):
    corrs=np.correlate(signal, t, mode='valid')/2
    maxcorrs=np.argwhere(corrs == np.amax(corrs)).flatten().tolist()
    counter=0
    for i in range(len(maxcorrs)-1):
        if maxcorrs[i]-maxcorrs[i+1]==CP*Nfft:
            counter+=1
        if counter==9:
            #we also need to check the power at maxcorrs to avoid flat signals
            return True
        return False
        