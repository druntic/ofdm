import FFT
import demodulate as demod
import BER as ber
import math
import numpy as np
import channel
import matplotlib.pyplot as plt
import packageDetection
import frequency
import time
import SCO
import sys
import modulate as mod
import fracDelay as fr
from matplotlib import pylab
from scipy import signal as sg
import itertools
import PAPR
import channelCoding as ch
#load settings
settings=np.loadtxt('settings.txt')
[Nfft, CP, M, fs, fo , SNR, K, Npilots]=settings
[Nfft, M, fs, fo, Npilots]=[int(x) for x in [Nfft, M, fs, fo, Npilots]]
Nfft=64

N=int(math.log2(M))*(Nfft-Npilots)
pilotValue=np.loadtxt('pilotValue Standard.txt').view(complex)
customPilots=np.loadtxt('custom pilotsStandard.txt').astype(int).tolist()
channelType="Rician"


if __name__ == "__main__":
    
    signal=np.loadtxt('realTxSignal.txt').view(complex)

    #samples_interpolated = sg.resample_poly(signal, 10, 1)
    #signal=np.array([samples_interpolated[i*10+4] for i in range(len(signal))])
    nOFDM=100
    T1=np.loadtxt('LTS T.txt').view(complex)
    t=np.loadtxt('STS t.txt').view(complex)
    #signal field contains information about number of OFDM symbols and type of modulation
    SigField=np.loadtxt('Sig field.txt').view(complex)#this is not static and we wont know this irl, but for now lets assume that its known

    
    plt.figure()
    Pxx, W = pylab.psd(signal, NFFT=64, Fs=fs)
    corrs=np.correlate(signal, T1, mode='valid')
    plt.figure()
    plt.title("Cross correlation between T1 and signal")
    plt.plot(abs(corrs))
    
    plt.figure()
    plt.title("Cross correlation between t and signal")
    plt.plot(abs(np.correlate(signal, t, mode='valid')/np.max(np.correlate(signal, t, mode='valid'))))


    plt.figure()
    plt.title("Signal on Rx")
    plt.plot([x.real for x in signal])

    
    #start_time = time.time()
    detected1, counter =packageDetection.detectPackage(signal, t, CP, Nfft)
    #print("package detected: ", detected1, "number of peaks: ", counter)#package detection using cross corr
    detected2, peak= packageDetection.detectPackage2(signal)#package detection using auto corr
    #print("package detected2: ",detected2, "peak at: ", peak)
    #np.savetxt('ComplexStream.txt', signal.view(float))#save signal to file
    #fileSignal=np.loadtxt('ComplexStream.txt').view(complex)
    #fileSignal=signal
    #print("--- %s seconds ---" % (time.time() - start_time))
    
    
    if detected1 and detected2:
        plt.close('all')
        print("package detected!")
        #remove sampling fractional offset
        #signal=frequency.correctFSFO(signal, T1)
        
    
        #remove CFO
        fileSignal, alphaCoarse=frequency.correctCoarseCFO(signal, CP, Nfft, fs,peak)    
        #find cross correlation after coarse CFO correction in order to find peak of T1 and do fine CFO correction    
        corrs=np.correlate(fileSignal, T1, mode='valid')
        plt.figure()
        plt.title("Cross correlation between T1 and signal after coarse CFO correction")
        plt.plot(abs(corrs))
        fileSignal=frequency.correctFineCFO(fileSignal, CP, Nfft, fs, corrs)
        
        
        start=packageDetection.T1startAfterCFO(abs(corrs))-32#index of start of LTS 
        
        
        plt.figure()
        plt.title("Cross correlation between t and signal after CFO correction")
        print("start of T1:",packageDetection.T1startAfterCFO(abs(corrs)))
        plt.plot(abs(np.correlate(fileSignal, t, mode='valid')/np.max(np.correlate(fileSignal, t, mode='valid'))))
    
        
        H1=channel.estimateChannel2(start+32, fileSignal, T1)#channel estimation using LTS
        #remove training symbols
        #extract time Signal Field signal
        LTSsig=fileSignal[start+64:start+128].copy()
        timeSigField=fileSignal[start+160:start+320]
        #demodulate the Signal Field
        pilotValue=FFT.StandardPilots(0, Npilots)
        SigField1=ch.demodulateSigField(timeSigField[:80], Nfft, CP, Npilots, pilotValue, customPilots, H1)
        pilotValue=FFT.StandardPilots(1, Npilots)
        SigField2=ch.demodulateSigField(timeSigField[80:], Nfft, CP, Npilots, pilotValue, customPilots, H1)
        
        #print("encoded sig field",SigField1+SigField2)
        #decode
        SigField=ch.decode(SigField1+SigField2).tolist()
        #print("decoded sig field",[int(i) for i in SigField])
        #extract modulation type and number of OFDM symbol from the signal field
        M, nOFDM=ch.getInfo(SigField)
        print("modulation:",M,"number of OFDM symbols:", nOFDM)
        if M==-1:
            sys.exit('something went wrong')
        
        
        fileSignal=fileSignal[start+320:start+320+int((CP*Nfft+Nfft)*nOFDM)]
    
        #recSignalField=fileSignal[:80].copy()#get signal field, for now we will assume that we got packet length and modulation type from the signal
        #fileSignal=fileSignal[80:]#remove signal field
        
        #split the signal into subarrays(symbols)
        dataSymbols=[fileSignal[x:x+int(Nfft+CP*Nfft)] for x in range(0, len(fileSignal), int(Nfft+CP*Nfft))]
        #our final bits will be saved in constcutedBits
        constructedBits=[]
        #save the individual cons in allcons in case we want to plot them
        allcons=[]
        plt.figure()
        plt.title("constellations")
        i=0
        
    
        
        for signal in dataSymbols:
            #take samples from cyclic prefix to compensate for fractional delay
            inside=2
            rec=signal[int(Nfft*CP)-inside:int(Nfft*CP)+Nfft-inside]
            rec=np.hstack([rec[inside:],rec[:inside]])
            
            #convert to frequency domain
            rec2cons=FFT.FFT(rec)
            
            #reverse the IFFT input
            rec2cons=np.hstack([rec2cons[38:64],rec2cons[1:27]])
            
            #deinterleave
            #rec2cons=PAPR.PAPRdemodulate(rec2cons, 4)
            
            
            #correct any residual CFO
            pilotValue=FFT.StandardPilots(i, Npilots)
            rec2cons=frequency.residualCFO(rec2cons, customPilots, pilotValue, H1)
            #equalize
            #H=channel.estimateChannel(rec2cons, customPilots, pilotValue, 52,H1)#find channel estimation using interpolation between pilots
            rec2cons=channel.equalizeChannel(rec2cons, H1)
            
            #correct SFO, not working and cant be tested for now
            # if i>0:
            #     eps0=frequency.findSFO(Z, customPilots, pilotValue)
            #     print(eps0)
            #     rec2cons=frequency.correctSFO2(eps0, rec2cons, i)
            # else:
            #     rec2cons=frequency.correctSFO2(LTSslope, rec2cons, i)
            # Z=rec2cons
            
            consNoPilots=FFT.RemovePilots(rec2cons, Nfft, Npilots,customPilots)
     
            allcons+=(consNoPilots)
            
            #plot constellations
            plt.plot(np.real(consNoPilots), np.imag(consNoPilots), ".", color="blue",markersize=10)
       
            cons2bits=demod.cons2bits(consNoPilots, M)
            constructedBits+=cons2bits
            i+=1
            if i==nOFDM:
                frequency.plotSlope(rec2cons, customPilots, pilotValue)
                plt.title("phase offset on each data subcarrier for symbol "+str(i))

        #decode
        print(len(constructedBits))
        #constructedBits=ch.decode(np.array(constructedBits), puncture=(False)).tolist()
        #save bits
        output = open("constructedBits.txt", "w")
        for x in constructedBits:
            output.write(str(x))
        output.close()
        with open('inputBits16.txt') as f:
            inputBits = f.readlines()
        f.close()
        if inputBits[0][-1]=="\n":
            inputBits[0]=inputBits[0][:-1]
        randomList=[int(i) for i in str(inputBits[0])]
        print("BER:", ber.BER(constructedBits ,randomList))
        #print("BER:", ber.BER(ch.decode(np.array(constructedBits), puncture=(False)).tolist() ,ch.decode(np.array(randomList), puncture=(False)).tolist()))

    else:
        print("no package detected")