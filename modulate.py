import numpy as np
import komm as km
import math
import itertools
import time

def getSymbols(parallelList,n):#convert binary list to a constellation
    symbols=[]
    for a in parallelList:
        x=km.binlist2int(a)
        if n==2:
            x_deg=x*360/n+360/n
        elif n==4:
            x_deg=x*360/n+360/(2*n)
        else:
            x_deg=x*360/n
        #print(x_deg)
        x_rad=x_deg*np.pi/180
        symbols.append(round(np.cos(x_rad),3)+1j*round(np.sin(x_rad),3))
        
    return symbols

def generateCons(M):
    lst = list(itertools.product([0, 1], repeat=int(math.log2(M))))
    Map=[]
    for x in lst:
        for a in x:
            Map.append(a)
    if M==2:
        cons=BPSK(Map)
    elif M==8:
        cons=PSK8(Map)
    else:
        cons=QAM(Map, M)
    return cons

def BPSK(binlist):
    paddedList=binlist
    #check if proper length
    if len(paddedList)%1!=0:
        paddedList.append(0)
    parallelList=np.array_split(paddedList, len(paddedList)/1)
    #print(parallelList)    
    symbols=getSymbols(parallelList, 2)
    return symbols
    
def QPSK(binlist):
    paddedList=binlist
    #check if proper length
    while len(paddedList)%2!=0:
        paddedList.append(0)
    parallelList=SP(binlist,4)  
    symbols=getSymbols(parallelList, 4)
    return symbols
        
def PSK8(binlist):
    paddedList=binlist
    #check if proper length
    while len(paddedList)%3!=0:
        paddedList.append(0)
    parallelList=SP(binlist,8)
    #print(parallelList)    
    symbols=getSymbols(parallelList, 8)
    return symbols

def SP(binList,M):#serial to parallel
    s1=math.log2(M)
    paddedList=binList
    #check if proper length
    # while len(paddedList)%s1!=0:
    #     paddedList.append(0)
    #paddedList.extend([0]*(len(paddedList)%s1))
    parallelList=np.array_split(paddedList, len(paddedList)/s1)

    return parallelList

def PS(parallelList):
    return np.concatenate(parallelList)

def QAM(binlist,M):
    
    K=1
    if M==4:
        K=np.sqrt(2)/2
    elif M==16:
        K=np.sqrt(10)/10
    elif M==64:
        K=np.sqrt(42)/42
    D=np.sqrt(M)
    parallelList=SP(binlist,M)
    
    symbols=[]
    for n in parallelList:
        n=n.tolist()
        ndec=km.binlist2int(n)
        a=ndec ^(ndec >> 1)#gray code
        (x,y)=np.divmod(a,D) #element-wise quotient and remainder
        Ax=2*x+1-D # PAM Amplitudes 2d+1-D - real axis
        Ay=2*y+1-D # PAM Amplitudes 2d+1-D - imag axis

        constellation = (Ax+1j*Ay)*K 
        symbols.append(constellation)
    
    
    return symbols
    
def modulate(randomList,M):
    if M==2:
        bits2cons=BPSK(randomList)
    elif M==8:
        bits2cons=PSK8(randomList)
    else:
        bits2cons=QAM(randomList, M)
    return bits2cons


def Minfo(M):
    Mcode = {
        2: 0,
        4: 1,
        8: 2,
        16: 3,
        64: 4,
        256: 5
    }
    return Mcode[M]

def infoM(M):
    Mcode = {
        0: 2,
        1: 4,
        2: 8,
        3: 16,
        4: 64,
        5: 256
    }
    return Mcode[M]
