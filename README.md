**HOW TO RUN:**

1. Run modulator.py in order to make the signal.
-  Modulator loads settings from settings.txt as:

1. Number of data subcarriers
2. Cyclic prefix ratio
3. Type of modulation (2,4,8,16,64)
4. Sampling rate
5. Frequency offset
6. SNR
7. Rician K factor
8. Number of Pilots
- pilotValue Standard.txt contains the standard pilot values for 802.11a, permutation still needs to be implemented.

- custom pilotsStandard.txt contains the standard incides for 802.11a pilots, (-21,-7,7,21).
- Random bits are generated in randomListDecoded and saved to inputBits.txt for ber comparison.
The generated signal is saved in TxSignal.txt.



2. Run binaryTesting.py in order to convert TxSignal.txt into binary signal for USRP. The binary file is saved in TxSignal.

3. Plug TxSignal in GNU Radio and begin transmission... save to TxSignalRec.

4. Run binaryTesting.py again to read signal and save it as realTxSignal.txt.

5. Run demodulator.py.

6. (Optional) controller.py is used to run modulator and demodulator and simulate real life transmission.
