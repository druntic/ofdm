
# while True: 
#     #runtime..
#     answer = input("ENTER something to quit: ") 
#     if answer: 
#         break 
import math
from random import randint
import numpy as np
import BER as ber
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import packageDetection


settings=np.loadtxt('settings.txt')
[Nfft, CP, M, fs, fo , SNR, K, Npilots]=settings
[Nfft, M, fs, fo, Npilots]=[int(x) for x in [Nfft, M, fs, fo, Npilots]]
Nfft=64
#Nfft=52#usable Nfft
N=int(math.log2(M))*(Nfft-Npilots)
pilotValue=np.loadtxt('pilotValue.txt').view(complex)
customPilots=np.loadtxt('custom pilots.txt').astype(int).tolist()
channelType="Rician"

# create empty lists for the x and y data
signal=np.loadtxt('TxSignal.txt').view(complex)
nPacket=1
signals=np.array([])
noises=[]

for i in range(nPacket):
    noise=np.array(ber.recievedSignal([], SNR, M, channelType, K))
    packet=np.array(ber.recievedSignal(signal, SNR, M, channelType, K))
    signals=np.hstack([signals,noise,packet,noise ])
signal=signals
signal=signal[::2]
x=[]
y=[]

x1=[i for i in range(len(signal))]
# create the figure and axes objects
fig, ax = plt.subplots()


def autocorrs(signal):
    L=16
    D=16
    
    c=[]
    p=[]
    m1=[]
    for n in range(len(signal)-32):
       c.append(sum([signal[n+k]*np.conjugate(signal[n+k+D]) for k in range(L)]))
       p.append(sum([signal[n+k+D]*np.conjugate(signal[n+k+D]) for k in range(L)]))
       m1.append(abs(c[n])**2/(p[n]**2))
    return m1

autocorr=autocorrs(signal)

def animate(i):

    x.append(i)
    y.append(autocorr[i].real)
    
    # if len(y)>160:
    #     detected, peak= packageDetection.detectPackage2(np.array(y))
    #     print("package detected2: ",detected)
    ax.clear()
    ax.plot(x, y)
    ax.set_xlim([i-100,i+100])
    #ax.set_ylim([np.min(signal),np.max(signal)])
    ax.set_ylim([np.min(0),np.max(autocorr)])



ani = animation.FuncAnimation(fig, animate, frames=len(signal), interval=0, repeat=False)

plt.show()


