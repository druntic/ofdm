
import sk_dsp_comm.fec_conv as fec
import sk_dsp_comm.digitalcom as dc
import BER as ber
import numpy as np
import matplotlib.pyplot as plt
cc1 = fec.FECConv(('110011','111000', '111010'),25)
cc2 = fec.FECConv(('110011','111000'),25)
# Encode with shift register starting state of '00'
state = '000000000'
EbN0 = -5
# Create 100000 random 0/1 bits
with open('inputBits.txt') as f:
    x = f.readlines()
f.close()
x=[int(i) for i in str(x[0])]

BERs=[]
enBERs3=[]
enBERs2=[]
iters=1
plt.close('all')
for EbN0 in range(-10,10):
    sum1=0
    sum2=0
    sum3=0
    for i in range(iters):
        y,state = cc1.conv_encoder(x,state)
        y=y.astype(int)
        yn_soft = dc.cpx_awgn(2*y-1,EbN0-3,1) # Channel SNR is 3 dB less for rate 1/2
        yn_hard = ((np.sign(yn_soft.real)+1)/2).astype(int)
        z = cc1.viterbi_decoder(yn_hard,'hard')

        sum1+=ber.BER(y, yn_hard)
        sum3+=ber.BER(z.astype(int).tolist(),x)
        
        y2,state = cc2.conv_encoder(x,state)
        y2=y2.astype(int)
        yn_soft = dc.cpx_awgn(2*y2-1,EbN0-3,1) # Channel SNR is 3 dB less for rate 1/2
        yn_hard = ((np.sign(yn_soft.real)+1)/2).astype(int)
        z2 = cc2.viterbi_decoder(yn_hard,'hard')
        
        
        sum2+=ber.BER(z2.astype(int).tolist(),x)
    BERs.append(sum1/iters)
    enBERs3.append(sum3/iters)
    enBERs2.append(sum2/iters)
    print("BER: ", BERs[-1])
    print("decoded BER with R=1/3: ", enBERs3[-1])
    print("decoded BER with R=1/2: ", enBERs2[-1])
    
plt.plot([i for i in range(len(BERs))],BERs) 
plt.plot([i for i in range(len(BERs))],enBERs3)
plt.plot([i for i in range(len(BERs))],enBERs2)
plt.legend(["BER no encoding", "BER with R=1/3 encoding","BER with R=1/2 encoding"])
plt.grid()
plt.show()