import scipy.interpolate
import matplotlib.pyplot as plt
import numpy as np
import FFT

def estimateChannel(rec2cons,pilots, pilotValue, N,Hest1):
    recPilotValues=rec2cons[pilots]
    Hest_at_pilots = recPilotValues / pilotValue
    Hest_abs = scipy.interpolate.interp1d(pilots, abs(Hest_at_pilots), kind='linear')([i for i in range(N)])
    Hest_phase = scipy.interpolate.interp1d(pilots, np.angle(Hest_at_pilots), kind='linear')([i for i in range(N)])
    Hest = Hest_abs * np.exp(1j*Hest_phase)
    # plt.figure()
    # plt.stem(pilots, abs(Hest_at_pilots), label='Pilot estimates')
    # plt.plot([i for i in range(N)], abs(Hest), label='Estimated channel via interpolation')
    # plt.plot([i for i in range(N)], abs(np.array(Hest1)), label='Estimated channel via LTS')
    
    # plt.grid(True); plt.xlabel('Carrier index'); plt.ylabel('$|H(f)|$'); plt.legend(fontsize=10)
    # # plt.ylim(0,2)
    return Hest


def estimateChannel2(start,fileSignal,T1):#estimate channel using LTS

    LTS2cons=[1, 1, -1, -1, 1, 1, -1, 1, -1, 1, 1, 1, 1, 1, 1, -1, -1, 1, 1, -1, 1, -1, 1, 1, 1, 1, 0,1, -1, -1, 1, 1, -1, 1, -1, 1, -1, -1, -1, -1, -1, 1, 1, -1, -1, 1, -1, 1, -1, 1, 1, 1, 1]
    Lright=LTS2cons[:26]
    Lleft=LTS2cons[27:]
    LTS2cons=[0]+Lleft+[0,0,0,0,0,0,0,0,0,0,0]+Lright
    RxT1=FFT.FFT(fileSignal[start:start+64])
    Rx2T1=FFT.FFT(fileSignal[start+64:start+64+64])
    RxT1=0.5*(RxT1+Rx2T1)
    TxT1=FFT.FFT(T1)
    RxT1=np.hstack([RxT1[38:64],RxT1[1:27]])
    TxT1=np.hstack([TxT1[38:64],TxT1[1:27]])
    Hest=[y/x for y,x in zip(RxT1,TxT1)]
    # Hest_abs=abs(np.array(Hest))
    # Hest_phase=np.angle(Hest)
    # Hest = Hest_abs * np.exp(1j*Hest_phase)
    
    # plt.figure()
    # plt.plot([i for i in range(52)], abs(np.array(Hest)), label='Estimated channel via LTS')
    # plt.grid(True); plt.xlabel('Carrier index'); plt.ylabel('$|H(f)|$'); plt.legend(fontsize=10)
    
    #print(np.around(Hest,2))
    return Hest
    

def equalizeChannel(signal, H):
    return signal/H

def plotChannels(Hest,Hest1,rec2cons, pilots, pilotValue,N):
    recPilotValues=rec2cons[pilots]
    Hest_at_pilots = recPilotValues / pilotValue
    plt.figure()
    plt.stem(pilots, abs(Hest_at_pilots), label='Pilot estimates')
    plt.plot([i for i in range(N)], abs(Hest), label='Estimated channel via interpolation')
    plt.plot([i for i in range(N)], abs(np.array(Hest1)), label='Estimated channel via LTS')
    plt.grid(True); plt.xlabel('Carrier index'); plt.ylabel('$|H(f)|$'); plt.legend(fontsize=10)
    