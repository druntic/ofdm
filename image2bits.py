import cv2
import numpy as np
import dec2bin
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import textwrap
import time

def decimalToBinary(n):
    return format(n, '#010b').replace("0b","")
def binaryToDecimal(n):
    return int(n,2)
def b2d(n):
    out = 0
    for bit in n:
        out = (out << 1) | int(bit)
    return out
def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

img = np.array(cv2.imread('pilots.png'))
img=np.flip(img, axis=-1)
 
oldshape=img.shape
imgvec=img.reshape(-1)

binvec=[decimalToBinary(x) for x in imgvec]

binserial=''.join(binvec)
text_file = open("inputBits.txt", "w")
n = text_file.write(binserial)
text_file.close()
print("bing")
exec(open("controller.py").read())
with open('constructedBits.txt') as f:
    inputBits = f.readlines()
f.close()

separatebin=textwrap.wrap(inputBits[0],8)
start_time = time.time()

print("boop")
decvec=[b2d(list(x)) for x in separatebin]


recoveredImg=np.array(decvec[:len(imgvec)]).reshape(oldshape)
print("--- %s seconds ---" % (time.time() - start_time))
imgplot = plt.imshow(recoveredImg)


