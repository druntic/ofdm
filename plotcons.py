import komm as km
import modulate as mod
import numpy as np
import matplotlib.pyplot as plt
import random
import math
import itertools
import demodulate as demod
import BER as ber
if __name__ == "__main__":

    
    FFT_N=64
    M=4
    N=int(math.log2(M))*FFT_N
    K=5#rician K factor
    SNR=20
    randomList=[random.randint(0, 1) for i in range(N)]

    

    lst = list(itertools.product([0, 1], repeat=int(math.log2(M))))
    #show constellations
    Map=[]
    for x in lst:
        for a in x:
            Map.append(a)
    cons=mod.QAM(Map,M)
    
    signal=mod.QAM(randomList,M)
    
    if M==2:
        cons=mod.BPSK(Map)
        signal=mod.BPSK(randomList)
    plt.plot(np.real(cons), np.imag(cons), '.', color="blue",markersize=10)
    print(cons)
    #generate noise and add it to signal    
    rec=ber.recievedSignal(signal, SNR, M, "AWGN",K)

    constructedSignal=[]
    for x in rec:
        minimum, binary =demod.deQAM(x,cons,M)
        if M==2:
            minimum, binary = demod.dePSK(x, cons, M)
        plt.plot(np.real(minimum), np.imag(minimum), ".", color="yellow",markersize=10)
        constructedSignal+=list(binary)

    print("SNR: ",SNR,"BER", ber.BER(constructedSignal, randomList))


    plt.plot(np.real(rec), np.imag(rec), '.', color="green",markersize=10)
    plt.grid(True)

    plt.gca().set_aspect('equal', adjustable='box')
    plt.show()
    