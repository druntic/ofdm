import numpy as np
from scipy import signal
import samplerate


real=np.fromfile("TxSignalRec", dtype="float32")[:20000000]

#simulate sampling frequency offset
#real = signal.resample(real, 80+20000000)
real=samplerate.resample(real, (20000000+80) / 20000000, 'sinc_best')  
real=real[100000:140000]

print(real)

realTx=np.array([])
for i in range(len(real)//2):
    realTx=np.hstack([realTx,real[2*i]+1j*real[2*i+1]])

np.savetxt('realTxSignal2.txt', realTx.view(float))