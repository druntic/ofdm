import numpy as np
import FFT

def PAPRmodulate(signal,k):
    KsubBands=np.array_split(signal,k)
    FFTbands=[FFT.FFT(x) for x in KsubBands]
    #interleave
    inter = np.ravel(np.column_stack(FFTbands))
    return inter

def PAPRdemodulate(inter, k):
    #deinterleave    
    deinterleaved = [inter[idx::k] for idx in range(k)]
    IFFTbands=[FFT.IFFT(x) for x in deinterleaved]
    return np.concatenate(IFFTbands)