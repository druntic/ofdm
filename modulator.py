import FFT
import modulate as mod
import math
import numpy as np
import random
import komm as km
import matplotlib.pyplot as plt
from matplotlib import pylab
import PAPR
import channelCoding as ch

#load simulation settings
settings=np.loadtxt('settings.txt')
[Nfft, CP, M, fs, fo , SNR, K, Npilots]=settings
[Nfft, M, fs, fo, Npilots]=[int(x) for x in [Nfft, M, fs, fo, Npilots]]
#Nfft is the number of data subcarriers, while Ntruefft is the number of data subcarriers with guard subcarriers
Ntruefft=64
#N is the number of bits per OFDM symbol
N=int(math.log2(M))*(Nfft-Npilots)
#values of pilots
pilotValue=np.loadtxt('pilotValue Standard.txt').view(complex)
#indices of pilots
PilotIndices=np.loadtxt('custom pilotsStandard.txt').astype(int).tolist()

channelType="Rician"
if __name__ == "__main__":
    #open text file containing binary sequence
    # with open('inputBits.txt') as f:
    #     inputBits = f.readlines()
    # f.close()

    
    # if inputBits[0][-1]=="\n":
    #     inputBits[0]=inputBits[0][:-1]
    # randomList=[int(i) for i in str(inputBits[0])]
    #randomList=np.loadtxt("Encoded bits.txt").astype(int).tolist()
    
    signal=[]
    #for simulations we will not read bit list yet
    nOFDM=50#number of data OFDM symbols, will be increasing in packet due to encoding
    randomList1=[random.randint(0, 1) for i in range(nOFDM*N)]#generate random bits
    
    #encode the bits
    randomList=ch.encode(randomList1, puncture=(False)).astype(int).tolist()
    print("coding rate", len(randomList1)/len(randomList))
    nOFDM=int(len(randomList)//(48*math.log2(M)))
    #pad if not divisible by N
    while(len(randomList)%N!=0):
        randomList.append(0)
    randomLists=[randomList[x:x+N] for x in range(0, len(randomList), N)]#separate bits to fit inside 1 OFDM symbol
    for i in range(len(randomLists)):
        #transform the bits into constellations
        bits2cons=mod.modulate(randomLists[i],M)#convert bit list to parallel lists each of length log(M) and assign each list a constellation
        pilotValue=FFT.StandardPilots(i, Npilots)
        consWithPilots=FFT.symbolsToOFDMsymbol(bits2cons, Nfft, Npilots, pilotValue,PilotIndices)#add pilots between constellations
        #interleave
        #consWithPilots=PAPR.PAPRmodulate(consWithPilots, 4)
        
        #transform the data for IFFT input
        cut=np.array_split(consWithPilots, 2)
        consWithPilots=np.hstack([cut[0],[0],cut[1]]).tolist()
        consright=consWithPilots[:26]
        consleft=consWithPilots[27:]
        consWithPilots=[0]+consleft+[0,0,0,0,0,0,0,0,0,0,0]+consright
        OFDMsym=FFT.IFFT(consWithPilots)#perform IFFT to get OFDM symbol
        CPrefix=FFT.CPrefix(OFDMsym,Ntruefft,CP)#make cyclic prefix of OFDM symbol
        
        if i>0:
            #create cyclic suffix in order to reduce spectral regrowth
            Csuffix=OFDMsymPast[0+int(CP*Nfft)]
            CPrefix[0]+=Csuffix#add suffix to prefix, we can do this only after the first OFDM symbol
            signal=np.hstack([signal,CPrefix, OFDMsym])
            #print(OFDMsym)
        else:
            #print(consWithPilots)
            signal=np.hstack([signal,CPrefix, OFDMsym])# create CP and add it at the start of the list
        OFDMsymPast=OFDMsym
        
    #print(len(signal))
    
    #create signal field containing type of modulation and length of packet
    #standard signal field structure: 4 bits for modulation type, 1 bit R for reverse bit, 12 bits for packet len
    #1 bit P for number of previous bits mod2, n zeros for padding 
    
    
    packetLength=nOFDM#number of ofdm symbols
    binPacketLen=km.int2binlist(packetLength).tolist()
    while(len(binPacketLen)<12):
        binPacketLen.append(0)
    binM=km.int2binlist(mod.Minfo(M)).tolist()#0:BPSK, 1: QPSK, 2:8PSK, 3:16QAM, 4:64QAM, 5:256QAM
    while(len(binM)<4):
        binM.append(0)

    signalField=(binM+[1]+binPacketLen+[0]+[0 for i in range(6)])
    print("decoded sig field:",signalField)
    codedSigField=ch.encode(signalField, SField=True)
    print("encoded sig field:",codedSigField)
    pilotValue=FFT.StandardPilots(0, Npilots)
    timeSigField1=ch.modulateSigField(codedSigField[:48], Nfft, Ntruefft, CP, Npilots, pilotValue, PilotIndices)
    pilotValue=FFT.StandardPilots(1, Npilots)
    
    timeSigField2=ch.modulateSigField(codedSigField[48:], Nfft, Ntruefft, CP, Npilots, pilotValue, PilotIndices)
    timeSigField=np.hstack([timeSigField1,timeSigField2])
    
    
    #create Long training sequence GI2+T1+T2, T1=T2 and add it before the signal
    
    LTS2cons=[1, 1, -1, -1, 1, 1, -1, 1, -1, 1, 1, 1, 1, 1, 1, -1, -1, 1, 1, -1, 1, -1, 1, 1, 1, 1, 0,1, -1, -1, 1, 1, -1, 1, -1, 1, -1, -1, -1, -1, -1, 1, 1, -1, -1, 1, -1, 1, -1, 1, 1, 1, 1]
    Lright=LTS2cons[:26]
    Lleft=LTS2cons[27:]
    LTS2cons=[0]+Lleft+[0,0,0,0,0,0,0,0,0,0,0]+Lright
    T1=FFT.IFFT(LTS2cons)
    
    LTS=[]
    for i in range(161):
        if i==0:
            LTS.append(0.5*T1[i-32])
        elif i==160:
            LTS.append(0.5*T1[(i-32)%len(T1)])
        else:
            LTS.append(T1[(i-32)%len(T1)])
    LTS=np.array(LTS)
    np.savetxt('LTS T.txt', T1.view(float))
    
    
    #create 10 short training symbols t and add them before the signal
    STS2cons=[0, 0, 1+1j, 0, 0, 0, -1-1j, 0, 0, 0, 1+1j, 0, 0, 0, -1-1j, 0, 0, 0, -1-1j, 0, 0, 0, 1+1j, 0, 0, 0, 0,0, 0, 0, -1-1j, 0, 0, 0, -1-1j, 0, 0, 0, 1+1j, 0, 0, 0, 1+1j, 0, 0, 0, 1+1j, 0, 0, 0, 1+1j, 0,0]
    Sright=STS2cons[:26]
    Sleft=STS2cons[27:]
    STS2cons=[0]+Sleft+[0,0,0,0,0,0,0,0,0,0,0]+Sright
    STS2cons=[math.sqrt(13/6)*x for x in STS2cons]
    t=FFT.IFFT(STS2cons)
    STS=[]
    for i in range(161):
        if i==0:
            STS.append(0.5*t[i])
        elif i==160:
            STS.append(0.5*t[(i)%len(t)])
        else:
            STS.append(t[(i)%len(t)])
    STS=np.array(STS)
    np.savetxt('STS t.txt', t[:16].view(float))
    
    
    
    
    #merge STS, LTS and signal field with OFDM symbols to form packet
    LTS[0]+=STS[-1]
    signal[0]+=LTS[-1]
    STS=STS[:-1]
    LTS=LTS[:-1]
    signal=np.hstack([STS,LTS,timeSigField,signal])
    
    
    #np.savetxt('TxSignal.txt', signal.view(complex))#save signal to file
    np.savetxt('TxSignal.txt', signal.view(float))
    #output will be used to check BER
    output = open("inputBits16.txt", "w")
    for x in randomList:
        output.write(str(x))
    output.close()
    
    #plot the final signal in frequency domain
    #plt.figure()
    #Pxx, W = pylab.psd(signal, NFFT=64, Fs=fs)
