import numpy as np
import scipy.signal as sp


def allp_delayfilt(tau): 
    #produces a Fractional-delay All-pass Filter Arguments: tau: fractional delay in samples (float). :returns: a: Denominator of the transfer function b: Numerator of the transfer function 
    L= int(tau)+1
    n = np.arange(0,L)
    print(n)
    a_0 = np.array([1.0]) 
    a= np.array(np.cumprod( np.divide(np.multiply((L - n), (L-n-tau)) , (np.multiply((n+1), (n+1+tau))))))
    a = np.append(a_0, a) # Denominator of the transfer function

    b = np.flipud(a) # Numerator of the transfer function eturn a, b 
    return a,b

def delaySignal(signal, tau):
    a,b=allp_delayfilt(tau)
    y=sp.lfilter(b,a,signal)
    return y


def frac_delay_fir(ntaps, u):
    eps = np.finfo(np.float32).eps
    if (u % 1) == 0:
        u = u + eps; # eps= 2.2e-16 prevent divide by zero

    N = ntaps - 1
    n = np.arange(-N//2, N//2 + 1)
    sinc = np.sin(np.pi * (n-u)) / (np.pi * (n-u)) # truncated impulse response
    win = sp.chebwin(ntaps, 70) # window function
    b = sinc * win              # apply window function
    return b