import numpy as np
import komm as km
import math
import modulate as mod
import time
import scipy
import matplotlib.pyplot as plt


def getIntFromSymbol(a, n):
    
    #x_rad=math.atan(np.imag(a)/np.real(a))
    x_rad=np.angle(a)
    x_deg=x_rad*180/np.pi
    if x_deg<0:
        x_deg+=360
    #print(x_deg)
    if n==8:
        x=x_deg*n/360
    elif n==4:
        x=n*(x_deg-360/(2*n))/360
    elif n==2:
        x=n*(x_deg-360/n)/360
    #print("int is:", int(x))
    return int(x)
def distance(X1,X2):
    return math.sqrt((np.real(X1)-np.real(X2))**2+(np.imag(X1)-np.imag(X2))**2)    

def inversegrayCode(n):
    inv = 0;
     
    # Taking xor until
    # n becomes zero
    while(n):
        inv = inv ^ n;
        n = n >> 1;
    return inv;

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]

def deQAM(point, cons, M):
    K=1
    if M==4:
        K=np.sqrt(2)/2
    elif M==16:
        K=np.sqrt(10)/10
    elif M==64:
        K=np.sqrt(42)/42
    
    D=np.sqrt(M)
    minimum=find_nearest(cons, point)
    minimum/=K
    
    #print("minimum: ",minimum)
    #start_time = time.time()
    # for x in cons:
    #     if distance(point,x)<distance(point,minimum):
    #         minimum=x
    #print("--- %s seconds ---" % (time.time() - start_time))    
    x=(np.real(minimum)-1+D)/2
    y=(np.imag(minimum)-1+D)/2
    a=int((x*D+y))

    N=int(math.log2(M))
    lista=list(km.int2binlist(inversegrayCode(a)))
    while len(lista)<N:
        #print("+")
        lista.append(0)
    
    return minimum, lista
            
def dePSK(point, cons, M):
    minimum=cons[0]
    for x in cons:
        if distance(point,x)<distance(point,minimum):
            minimum=x
    if M==2:
        if minimum==1:
            return minimum,[1]
        return minimum,[0]
    N=math.log2(M)
    #print("min: ", minimum)
    lista=list(km.int2binlist(getIntFromSymbol(minimum, M)))
    while len(lista)<N:
        #print("+")
        lista.append(0)
    return minimum, lista


#generate cons for each type of modulation
cons=[mod.generateCons(2),mod.generateCons(4),mod.generateCons(8),mod.generateCons(16),mod.generateCons(64),mod.generateCons(256)]

def cons2bits(consNoPilots,M):
   
    c2b=[]
    #start_time = time.time()
    for x in consNoPilots:
        if M==2 or M==8:
            minimum, binary = dePSK(x, cons[mod.Minfo(M)], M)
        else:
            minimum, binary =deQAM(x,cons[mod.Minfo(M)],M)
        c2b+=list(binary)
    #print("total %s ms " % ((time.time() - start_time)*1000))
    return c2b