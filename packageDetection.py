import numpy as np
import matplotlib.pyplot as plt
def detectPackage(signal,t, CP, Nfft):#using cross correlation with STS
    corrs=np.correlate(signal, t, mode='valid')/np.max(np.correlate(signal, t, mode='valid'))
    maxcorrs=np.argwhere(abs(corrs) >= 0.7).flatten().tolist()
    #print("maxcorrs: ", maxcorrs)
    counter=0
    for i in range(len(maxcorrs)-1):
        if maxcorrs[i+1]-maxcorrs[i]==CP*Nfft:
            counter+=1
    
    if counter>=9:
            #we also need to check the power at maxcorrs to avoid flat signals
    
        return True, counter
    return False, counter
        
def T1start(corrs):
    if (np.max(corrs)-1)<=0.2:
        corrs2=corrs.copy()
    else:
        corrs2=corrs.copy()/2
    test=np.argwhere(corrs2>0.55)
    treshold=0.6
    #print("starts: ", np.argwhere(corrs2>0.7))
    for i in test:
        #print("test[i]",i[0])
        if corrs2[i[0]-64]>treshold and corrs2[i[0]]>treshold:
                if corrs2[i[0]+64]>treshold and corrs2[i[0]]>treshold:
                    return i[0]
                return i[0]-64


def T1startAfterCFO(corrs):
    
    if (np.max(corrs)-1)<=0.2:
        corrs2=corrs.copy()
    else:
        corrs2=corrs.copy()/2
    #print(max(corrs2))
    treshold=0.6
    test=np.argwhere(corrs2>treshold)
    for i in test:
        if corrs2[i[0]-64]>treshold and corrs2[i[0]]>treshold:
                if corrs2[i[0]+64]>treshold and corrs2[i[0]]>treshold:
                    print("start:", i[0])
                    return i[0]
                #print("start:", i[0]-64)
                return i[0]-64
    
    # first=np.argmax(corrs2)
    # corrs2=np.delete(corrs2, [first])
    # second=np.argmax(corrs2)
    # if first<second:
    #     return first
    # return second


def detectPackage2(signal):#using auto correlation and sliding algorithm
    L=16
    D=16
    
    c=[]
    p=[]
    m1=[]

    for n in range(len(signal)-32):
        c.append(sum([signal[n+k]*np.conjugate(signal[n+k+D]) for k in range(L)]))
        p.append(sum([signal[n+k+D]*np.conjugate(signal[n+k+D]) for k in range(L)]))
        m1.append(abs(c[n])**2/(p[n]**2))
       
    #m1=[abs(sum([signal[n+k]*np.conjugate(signal[n+k+D]) for k in range(L)]))**2/(sum([signal[n+k+D]*np.conjugate(signal[n+k+D]) for k in range(L)]))**2 for n in range(len(signal)-32)]
     
    #plt.close('all')
    # plt.figure()
    # plt.title("autocorrs")
    # plt.grid()
    # plt.plot(m1)
    
    m=SlidingAverage(m1)
    peak=ReverseSliding(m, m1)
    
    r=320#window length
    
    for i in range(len(m)-r):
        if abs(m[i]-m[(i+r)%len(m)])<0.1:
            if i+r>len(m):
                peak=np.max(m[i:]+m[:(i+r)%len(m)])
            else:
                peak=np.max(m[i:(i+r)%len(m)])
            if peak>4*m[i]:
                #print("peak at:", np.where(m==peak)[0][0])
                return True, ReverseSliding(m, m1)
    return False, ReverseSliding(m, m1)


def AutoCrossCrit1(m, r):#find segment of length r where all correlations are bigger than 0.6
    for i in range(len(m)):
        if m[i]>0.6:
            if all(x>0.6 for x in m[i-r:i+r]):
                return True
    return False
                
def SlidingAverage(arr):
        # Program to calculate moving average
    window_size = 160
      
    i = 0
    # Initialize an empty list to store moving averages
    moving_averages = []
      
    # Loop through the array to consider
    # every window of size 3
    while i < len(arr) - window_size + 1:
        
        # Store elements from i to i+window_size
        # in list to get the current window
        window = arr[i : i + window_size]
      
        # Calculate the average of current window
        window_average = round(sum(window) / window_size, 2)
          
        # Store the average of current
        # window in moving average list
        moving_averages.append(window_average)
          
        # Shift window to right by one position
        i += 1
    
    # plt.figure()
    # plt.title("sliding average")
    # plt.plot(moving_averages)
    return moving_averages


def ReverseSliding(moving_averages, original):
    arr=np.array(moving_averages)
    peakAt=np.argmax(arr)
    #print("peak at: ", peakAt,"is", arr[peakAt])
    #print("constructed value:", sum(original[peakAt:peakAt+160])/160)
    return peakAt

