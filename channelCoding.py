import numpy as np
from pyldpc import make_ldpc, encode, decode, get_message
import modulate as mod
import FFT
import frequency
import channel
import demodulate as demod
import komm as km
import modulate
import sk_dsp_comm.digitalcom as dc
import sk_dsp_comm.fec_conv as fec

def encode(SigField, puncture=False, SField=False):
    
    cc1 = fec.FECConv(('11101','10011'),7)
    state = '0000'
    if SField==True:
        x=SigField+[0 for i in range(len(SigField))]
    else:
        x=SigField
    y,state = cc1.conv_encoder(x,state)
    if puncture==False:
        #coding rate K=1/2
        return y
    else:
        #coding rate K=5/6
        return cc1.puncture(y, ('10101','11010'))
def decode(EnSigField, puncture=False):

    cc1 = fec.FECConv(('11101','10011'),7)
    state = '0000'
    if puncture==True:
        EnSigField=cc1.depuncture(EnSigField, ('10101','11010'), 1).astype(int)
    #print(EnSigField)
    return cc1.viterbi_decoder(np.array(EnSigField),'hard')
    


def modulateSigField(SigField,Nfft, Ntruefft, CP, Npilots, pilotValue,PilotIndices):
    bits2cons=mod.modulate(SigField,2)#convert bit list to parallel lists each of length log(M) and assign each list a constellation
    consWithPilots=FFT.symbolsToOFDMsymbol(bits2cons, Nfft, Npilots, pilotValue,PilotIndices)#add pilots between constellations

    
    #transform the data for IFFT input
    cut=np.array_split(consWithPilots, 2)
    consWithPilots=np.hstack([cut[0],[0],cut[1]]).tolist()
    consright=consWithPilots[:26]
    consleft=consWithPilots[27:]
    consWithPilots=[0]+consleft+[0,0,0,0,0,0,0,0,0,0,0]+consright
    OFDMsym=FFT.IFFT(consWithPilots)#perform IFFT to get OFDM symbol
    CPrefix=FFT.CPrefix(OFDMsym,Ntruefft,CP)#make cyclic prefix of OFDM symbol

    return np.hstack([CPrefix, OFDMsym])


#demodulate the signal field
def demodulateSigField(signal,Nfft, CP, Npilots, pilotValue,customPilots, H1):
    #take samples from cyclic prefix to compensate for fractional delay
    inside=2
    rec=signal[int(Nfft*CP)-inside:int(Nfft*CP)+Nfft-inside]
    rec=np.hstack([rec[inside:],rec[:inside]])
    
    #convert to frequency domain
    rec2cons=FFT.FFT(rec)
    
    #reverse the IFFT input
    rec2cons=np.hstack([rec2cons[38:64],rec2cons[1:27]])
    
    #deinterleave
    #rec2cons=PAPR.PAPRdemodulate(rec2cons, 4)
    
    
    #correct any residual CFO
    rec2cons=frequency.residualCFO(rec2cons, customPilots, pilotValue, H1)
    #equalize
    #H=channel.estimateChannel(rec2cons, customPilots, pilotValue, 52,H1)#find channel estimation using interpolation between pilots
    rec2cons=channel.equalizeChannel(rec2cons, H1)
    

    #rec2cons=frequency.correctSFO(rec2cons, customPilots, pilotValue)
    # if i>0:
    #     eps0=SCO.findSFO(Z, rec2cons, customPilots)
    #     rec2cons=SCO.SFOcorrection(rec2cons, eps0)
    # Z=rec2cons
    
    consNoPilots=FFT.RemovePilots(rec2cons, Nfft, Npilots,customPilots)

    return demod.cons2bits(consNoPilots, 2)

def getInfo(SigField):
    
    P=SigField[17]
    R=SigField[4]
    tail=np.array(SigField[18:23])
    
    if P!=R and np.all(tail==0):
        modulationInfo=SigField[:3]
        lengthInfo=SigField[5:16]
        return mod.infoM(km.binlist2int(modulationInfo)), km.binlist2int(lengthInfo)
    else:
        print("something went wrong")
        return -1, -1
