
import sk_dsp_comm.sigsys as ss
import scipy.signal as signal
from IPython.display import Audio, display
from IPython.display import Image, SVG
import scipy.special as special
import sk_dsp_comm.digitalcom as dc
import sk_dsp_comm.fec_conv as fec
import numpy as np
import BER as ber
cc1 = fec.FECConv(('111','101'),10)


N_bits_per_frame = 24
EbN0 = 30
cc1 = fec.FECConv(('11101','10011'),5)
# Encode with shift register starting state of '0000'
state = '0000'

x=np.array([1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]+[0 for i in range(24)])
print(x)
x = np.random.randint(0,2,48)
print(x)

with open('inputBits16.txt') as f:
    inputBits = f.readlines()
f.close()
if inputBits[0][-1]=="\n":
    inputBits[0]=inputBits[0][:-1]
x=[int(i) for i in str(inputBits[0])]
y,state = cc1.conv_encoder(x,state)
print("current code rate", len(x)/len(y))
yp = cc1.puncture(y, ('110','101'))
print("new code rate", len(x)/len(yp))
y=cc1.depuncture(yp, ('110','101'), 1)


print(len(y))
# Add channel noise to bits, include antipodal level shift to [-1,1]
yn_soft = dc.cpx_awgn(2*y-1,EbN0-3,1) # Channel SNR is 3 dB less for rate 1/2
yn_hard = ((np.sign(yn_soft.real)+1)/2).astype(int)
z = cc1.viterbi_decoder(yn_hard,'hard')
#print(np.array(x[:len(x)//2]))
#print(z.astype(int))
print(len(z))
print(ber.BER(z.astype(int),np.array(x) ))
