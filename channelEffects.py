import numpy as np
import BER as ber
import frequency


#load settings
settings=np.loadtxt('settings.txt')
[Nfft, CP, M, fs, fo , SNR, K, Npilots]=settings
[Nfft, M, fs, fo, Npilots]=[int(x) for x in [Nfft, M, fs, fo, Npilots]]
Nfft=64
channelType="Rician"

signal=np.loadtxt('TxSignal.txt').view(complex)
#simulate sending packets
nPacket=1
signal=ber.packet(signal, SNR, M, channelType, K, nPacket)

#apply frequency offset
signal=frequency.addCFO(signal, fo, fs)

np.savetxt('TxSignal.txt', signal.view(float))
