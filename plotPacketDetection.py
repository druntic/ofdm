import BER as ber
import math
import numpy as np
import matplotlib.pyplot as plt
import packageDetection



settings=np.loadtxt('settings.txt')
[Nfft, CP, M, fs, fo , SNR, K, Npilots]=settings
[Nfft, M, fs, fo, Npilots]=[int(x) for x in [Nfft, M, fs, fo, Npilots]]
Nfft=64
#Nfft=52#usable Nfft
N=int(math.log2(M))*(Nfft-Npilots)
pilotValue=np.loadtxt('pilotValue.txt').view(complex)
customPilots=np.loadtxt('custom pilots.txt').astype(int).tolist()
channelType="Rician"

if __name__ == "__main__":
    plt.close('all')
    signal=np.loadtxt('TxSignal.txt').view(complex)
    nSig=len(signal)
    T1=np.loadtxt('LTS T.txt').view(complex)
    t=np.loadtxt('STS t.txt').view(complex)
    
    autocounterCrit1=[]
    autocounterCrit2=[]
    autocounterCrit3=[]
    crosscounter=[]
    for SNR in range(-5,20):
        print(SNR)
        counter1=0
        counter2=0
        counter3=0
        counter4=0
        itera=50
        for i in range(itera):
            sig=[]
            noise=np.array(ber.recievedSignal([], SNR, M, channelType, K))
            #packet=np.array(ber.recievedSignal(signal, SNR, M, channelType, K))
            sig=np.hstack([sig,noise,noise,noise,noise ])
            if packageDetection.detectPackage(sig, t, CP, Nfft):
                counter1+=1
            if packageDetection.detectPackage2(sig):
                counter2+=1
            if packageDetection.detectPackage3(sig,5):
                counter3+=1
            if packageDetection.detectPackage3(sig,16):
                counter4+=1
        
        autocounterCrit1.append(counter2/itera)
        autocounterCrit2.append(counter3/itera)
        autocounterCrit3.append(counter4/itera)
        crosscounter.append(counter1/itera)
    
    plt.figure()
    plt.title("false positive")
    plt.plot(list(range(-5,20)),crosscounter)
    plt.plot(list(range(-5,20)),autocounterCrit1)
    plt.plot(list(range(-5,20)),autocounterCrit2)    
    plt.plot(list(range(-5,20)),autocounterCrit3)    
    
    plt.xlabel("S/N")
    plt.legend(["Cross correlation","Auto correlation Sliding","Auto correlation with segment r=5","Auto correlation with segment r=16"])
                