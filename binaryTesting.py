import numpy as np


#save
newFileBytes = np.loadtxt("TxSignal.txt").view(complex)#use encoded bitsv
print(newFileBytes)

#array_real, array_imag = np.loadtxt('TxSignal.txt', unpack=True)
#newFileBytes = array_real + 1j * array_imag
lista=np.array([])

for x,y in zip(newFileBytes.real,newFileBytes.imag):
    lista=np.hstack([lista,x,y])
print(lista)

lista.astype('float32').tofile('TxSignal')
test=np.fromfile('TxSignal', dtype=np.float32)
#print(newFileBytes)

print(test)


#read
real=np.fromfile("TxSignalRec", dtype="float32")[20000000:20040000]

print(real)

realTx=np.array([])
for i in range(len(real)//2):
    realTx=np.hstack([realTx,real[2*i]+1j*real[2*i+1]])

np.savetxt('realTxSignal.txt', realTx.view(float))