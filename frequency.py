import numpy as np
import packageDetection
import matplotlib.pyplot as plt
from scipy import signal as sg
import FFT
import channel

def addCFO(signal, fo, fs):
    Ts=1/fs
    #time = np.arange(0, Ts*len(signal), Ts) # create time vect
    time=np.array([Ts*i for i in range(len(signal))])
    return signal * np.exp(1j*2*np.pi*fo*time) # perform freq shift
    
    
    
def correctCFO(signal, CP, Nfft, fs): 
    Ts=1/fs
    #time = np.arange(0, Ts*len(signal), Ts) # create time vect
    time=np.array([Ts*i for i in range(len(signal))])
    l=int(CP*Nfft)#short training symbol length
    s=signal[int(5*l):int(10*l)]
    alphaCoarse= 1/l*np.angle(sum([np.conj(s[i])*s[i+l] for i in range(Nfft)] ))
    S=signal[int(10*l+2*l):int(10*l+2*l)+2*Nfft]
    S1=S*np.exp(-1j*alphaCoarse/(l*Ts)*time[0:(2*Nfft)])
    #print(S1)
    alphaFine=np.angle(sum([np.conjugate(S1[i])*S1[(i+Nfft)] for i in range(Nfft)]))/Nfft   
    f0coarse=alphaCoarse/(2*np.pi*Ts)
    f0fine=alphaFine/(2*np.pi*Ts*Nfft)
    f0total=f0coarse+f0fine#sum the offsets in order to get a better estimation
    print("fo coarse:", f0coarse)
    print("fo fine:", f0fine)
    print("fo final: ",f0total)
    alpha=alphaFine+alphaCoarse
    return signal * np.exp(-1j*2*np.pi*f0total*time)


def correctCoarseCFO(signal, CP, Nfft, fs,peak): 
    Ts=1/fs
    #time = np.arange(0, Ts*len(signal), Ts) # create time vect
    time=np.array([Ts*i for i in range(len(signal))])
    l=int(CP*Nfft)#short training symbol length
    s=signal[peak+int(5*l):peak+int(10*l)]
    
    
    alphasCoarse=[np.conjugate(s[i])*s[i+int(l)] for i in range(int(Nfft))]
    #print([np.angle(x)*fs/(2*np.pi*l) for x in alphasCoarse])
    
    f0coarse=sum([np.angle(x)*fs/(2*np.pi*l) for x in alphasCoarse])/Nfft
    # for i in range(Nfft):
    #     alpha=np.angle(np.conjugate(s[i])*s[i+l])/l
    #     print(alpha*fs/(2*np.pi))
    # print(alphaCoarse*fs/(2*np.pi))
    #alphaCoarse=np.angle(sum([np.conjugate(s[i])*s[i+int(l)] for i in range(int(Nfft))]))
    #f0coarse=alphaCoarse/(2*np.pi*Ts*(Nfft*CP))
    #print("fo coarse:", f0coarse)+
    alphanewCoarse= 1/16*np.angle(sum([np.conj(s[i])*s[i+16] for i in range(64)] ))
    f0coarse=fs*alphanewCoarse/(2*np.pi)
    print("f0 coarse new:", f0coarse)

    return signal * np.exp(-1j*2*np.pi*f0coarse*time), alphanewCoarse


def correctFineCFO(signal, CP, Nfft, fs, corrs):
    start=packageDetection.T1startAfterCFO(abs(corrs))-32#start of signal is start of T1 - GI2
    print("the start is: ", start)
    Ts=1/fs
    time=np.array([Ts*i for i in range(len(signal))])
    S=signal[start:start+2*Nfft].copy()

    alphaFine=np.angle(sum([np.conjugate(S[i])*S[(i+Nfft)] for i in range(Nfft)]))/Nfft
    #print([np.angle(np.conjugate(S[i])*S[(i+Nfft)])/(2*np.pi*Ts*Nfft*Nfft) for i in range(Nfft)])
    #alphaFine=sum([np.angle(np.conjugate(S[i])*S[(i+Nfft)])/Nfft for i in range(Nfft)])/Nfft
    f0fine=alphaFine/(2*np.pi*Ts)
    print("fo fine:", f0fine)
    return signal * np.exp(-1j*2*np.pi*f0fine*time)

def residualCFO(symbol, customPilots, pilotValues, H):
    j=0
    suma=0   
    #calculate angle at every pilot and get the mean, similar to coarse and fine CFO estimation

    for i in customPilots:
        suma+=pilotValues[j]*np.conj(symbol[i])*H[i] 
        j+=1
    beta=np.angle(suma)
    #print("residucal CFO phi: ",-beta)
    return symbol*np.exp(1j*beta)

#used for SFO correction of first OFDM symbol
def findLTSSFO(recT1, T1):
    equalizedLTS=[x*y for x,y in zip(recT1,T1)]
    angles=np.angle(equalizedLTS)
    plt.figure()
    plt.plot([i for i in range(len(T1))],angles)
    slope, intercept = np.polyfit(np.array([i for i in range(len(T1))]), angles, 1)
    print(slope)
    return slope
    
    
    
def findSFO(rec2cons, customPilots, pilotValue):
    #estimate phi using least squares
    equalizedPilots=[pilotValue[j]*rec2cons[i] for i,j in zip(customPilots,[0,1,2,3])]
    angles=np.angle(equalizedPilots)
    #plt.plot(customPilots,angles,"o" , color="green" )
    slope, intercept = np.polyfit(customPilots, angles, 1)
    return slope
    
#correct SFO using previous symbols
def correctSFO2(slope, symbol,l):
    K=(64+16)/64#for Nfft =64 and cp=0.25
    array=np.array([2*np.pi*l*K*slope*k for k in range(len(symbol))])
    return symbol*np.exp(-1j*array)    
    
def correctSFO(symbol, customPilots, pilotValues):
    j=0
    equalizedPilots=[]
    #equalize first
    for i in customPilots:
        #print(pilotValues[j]*pilotValues[j])
        equalizedPilots.append(pilotValues[j]*(symbol[i])) 
        j+=1
    
    #calculate angle at each pilot
    angles=[np.angle(x) for x in equalizedPilots]
    
    nAngles=angles[:2]
    pAngles=angles[2:]
    #print(nAngles, pAngles)
    #plt.figure()
    #plt.plot(customPilots,angles)
    deltaPangles=np.diff(nAngles)
    deltaNangles=np.diff(pAngles)
    deltaNindices=np.diff(customPilots[:2])
    deltaPindices=np.diff(customPilots[2:])
    
    #SFO estimation
    averagePAngle=np.mean([x/y for x,y in zip(deltaPangles, deltaPindices)])
    averageNAngle=np.mean([x/y for x,y in zip(deltaNangles, deltaNindices)])
    averageAngle=0.5*(averagePAngle-averageNAngle)
    
    
    #SFO correction
    sfo_corr=np.fft.fftshift(np.outer(np.arange(-26,26,1),(averageAngle)),0);
    sfo_corr=np.array([x[0] for x in sfo_corr])
    
    return symbol*sfo_corr

def correctFSFO(signal, T1):
    maxcorrs=[]
    m=10#how many new samples do you want between 2 samples
    samples_interpolated = sg.resample_poly(signal, m, 1)
    for k in range(m):
        #add fractional delay k to signal
        signal2=np.array([samples_interpolated[i*m+k] for i in range(len(signal))])
        corrs=np.correlate(signal2, T1, mode='valid')
        print("corr:", max(abs(corrs)), k)
        maxcorrs.append(max(abs(corrs)))
    #the signal with corrected fractional delay will have the biggest correlation between signal and T1
    return np.array([samples_interpolated[i*m+np.argmax(maxcorrs)] for i in range(len(signal))])

def plotSlope(rec2cons,customPilots, pilotValue):
    #plot phase offset of i-th symbol
    #we have 2 ways of finding the slope and intersection
    plt.figure()
    equalizedPilots=[pilotValue[j]*rec2cons[i] for i,j in zip(customPilots,[0,1,2,3])]
    #the angle between 2 neighboring subcarriers is phi
    #if the distance between subcarriers is k then the angle is phi/k
    diffAngles=[np.angle(np.conj(equalizedPilots[i])*equalizedPilots[i+1])/diff for i,diff in zip(range(len(equalizedPilots)-1), np.diff(customPilots))]
    #estimate phi using known pilots
    print("estimated SFO phi:", np.mean(diffAngles))
    plt.ylim([-3,3])
    #estimate phi using least squares
    angles=np.angle(equalizedPilots)
    plt.plot(customPilots,angles,"o" , color="green" )
    slope, intercept = np.polyfit(customPilots, angles, 1)
    print("estimated SFO phi:",slope)

    # Create a list of values in the best fit line
    abline_values = [slope * i+intercept   for i in range(len(rec2cons))]
    plt.plot([i for i in range(len(rec2cons))], abline_values, 'b')
    abline_values = [np.mean(diffAngles) * i   for i in range(len(rec2cons))]
    plt.xlabel("subcarrier")
    plt.ylabel("phi")
    

 