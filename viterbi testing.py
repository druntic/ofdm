import matplotlib.pyplot as plt
import sk_dsp_comm.fec_conv as fec
import sk_dsp_comm.digitalcom as dc
import BER as ber
import numpy as np


settings=np.loadtxt('settings.txt')
[Nfft, CP, M, fs, fo , SNR, K, Npilots]=settings
[Nfft, M, fs, fo, Npilots]=[int(x) for x in [Nfft, M, fs, fo, Npilots]]
cc1 = fec.FECConv(('11101','10011','10110'),25)
# Encode with shift register starting state of '0000'
state = '0000'
EbN0 = 1
# Create 100000 random 0/1 bits
with open('inputBits.txt') as f:
    x5 = f.readlines()
f.close()
x5=[int(i) for i in str(x5[0])]
y,state = cc1.conv_encoder(x5,state)
np.savetxt('Encoded bits.txt', y)

exec(open("modulator.py").read())
exec(open("demodulator.py").read())
test=np.loadtxt('Encoded bits.txt').astype(int)
# yn_soft = dc.cpx_awgn(2*y-1,EbN0-3,1) # Channel SNR is 3 dB less for rate 1/2
# yn_hard = ((np.sign(yn_soft.real)+1)/2).astype(int)
y=y.astype(int)

#z = cc1.viterbi_decoder(yn_hard,'hard')
# print("corrupter BER:", ber.BER(yn_hard.tolist(),y.tolist()))
# print("BER: ", ber.BER(z,x5))


plt.close('all')



with open('constructedBits.txt') as f:
    outputBits = f.readlines()
f.close()
constructedList=[int(i) for i in str(outputBits[0])]
print("encoded BER",ber.BER(y, constructedList))
print("data BER:", ber.BER(cc1.viterbi_decoder(np.array(constructedList),'hard'), x5))