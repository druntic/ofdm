import BER as ber
import numpy as np
import keyboard  # using module keyboard


if __name__ == "__main__":
    
    settings=np.loadtxt('settings.txt')
    [Nfft, CP, M, fs, fo , SNR, K, Npilots]=settings
    [Nfft, M, fs, fo, Npilots]=[int(x) for x in [Nfft, M, fs, fo, Npilots]]
    pltSig=[]
    # plt.figure()
    # plt.xlim([0,10000])
    # plt.ylim([0,1])
    while True:

        if keyboard.is_pressed('p'):
            break
        elif keyboard.is_pressed('v'):
            exec(open("modulator.py").read())
            #exec(open("binaryTesting.py").read())
        else:
            signal=np.array([])
            np.savetxt('TxSignal.txt', signal.view(float))
            
        exec(open("channelEffects.py").read())
        exec(open("demodulator.py").read())

    # exec(open("modulator.py").read())
    # exec(open("binaryTesting.py").read())
    # #exec(open("channelEffects.py").read())
    # exec(open("demodulator.py").read())

